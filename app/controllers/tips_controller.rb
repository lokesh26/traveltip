class TipsController < ApplicationController
	before_action :signed_in_user

  def index
  	@tips =Tip.all
  end

  def new
  	@tip = Tip.new
  end


  def create
  	@tip = Tip.new(user_params)
  	if @tip.save
  		flash[:success] = "Tip successfully saved!"
  		redirect_to action: 'index' 
  	else
  		render 'new'
  	end
  end

  private

    def user_params
      params.require(:tip).permit(:tip, :location)
    end
end

