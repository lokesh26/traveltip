class StaticPagesController < ApplicationController
	before_action :signed_in_user, only: :options

	def home
		if signed_in?
			render :options
		end
  	end

	def options
		
	end

end
