// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets
//= require turbolinks
//= require jquery.geocomplete.js
//= require_tree .


jQuery(function() {
  $('body').prepend('<div id="fb-root"></div>');
  return $.ajax({
    url: "" + window.location.protocol + "//connect.facebook.net/en_US/all.js",
    dataType: 'script',
    cache: true
  });
});


window.fbAsyncInit = function() {
  FB.init({
  appId: '333122010179759',
  //appId: '817545481613065',
    cookie: true
  });
  $('#sign_in').click(function(e) {
    e.preventDefault();
    return FB.login(function(response) {
      if (response.authResponse) {
        return window.location = '/auth/facebook/callback';
      }
    });
  });
  return $('#sign_out').click(function(e) {
    FB.getLoginStatus(function(response) {
      if (response.authResponse) {
        return FB.logout();
      }
    });
    return true;
  });
};

$(function(){
  $('#tiplocation').geocomplete();
    $('#searchlocation').geocomplete();

});
    

/*
function getlocation(){


    var input_two = document.getElementById('searchlocation');
    var input_one = document.getElementById('tiplocation');
    
       var autocomplete_two = new google.maps.places.Autocomplete(input_two);
    var autocomplete_one = new google.maps.places.Autocomplete(input_one);
 

    // listeners to capture updated location
    google.maps.event.addListener(autocomplete_one, 'place_changed', function() {
      var objLocation = autocomplete_one.getPlace();
    });
    google.maps.event.addListener(autocomplete_two , 'place_changed', function() {
      var objLocation = autocomplete_two.getPlace();
    });
}*/