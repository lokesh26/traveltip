Rails.application.routes.draw do

  root "static_pages#home"

  resources :tips
  match 'auth/:provider/callback', to: 'sessions#create', via: [:get, :post]
  match 'auth/failure', to: redirect('/'), via: [:get, :post]
  match 'signout', to: 'sessions#destroy', as: 'signout', via: [:get, :post]
  match 'options', to: 'static_pages#options', as: 'options', via: [:get, :post]

end
