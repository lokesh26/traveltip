class AddAddressToTips < ActiveRecord::Migration
  def change
    add_column :tips, :address, :string
  end
end
